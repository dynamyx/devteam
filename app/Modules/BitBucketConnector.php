<?php

namespace App\Modules;

use App\Repo;
use App\User;
use Carbon\Carbon;
use Github\Api\Repository\Hooks;
use Socialite;
use GuzzleHttp\Client as Guzzle;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Bitbucket\API\Repositories;
use Bitbucket\API\Http\Listener\OAuth2Listener as OAuth2;

use Tracy\Debugger;

class BitBucketConnector
{

    public $orgs = [];
    public $user = [];
    public $username = '';
    public $token = '';
    public $oAuth = '';

    public function __construct()
    {

        $this->user = Auth::user()->services['bitbucket'];

        $this->username = $this->user['nickname'];
        $this->token = $this->validateAccessToken();

        // a bit of normalization
        $this->user['login'] = $this->user['nickname'];
        $this->user['provider'] = 'bitbucket';

        $this->oAuth = new OAuth2(['access_token'  => $this->token]);

    }

    public function getOrgs()
    {

//        $groups = new \Bitbucket\API\Groups();
//        $groups->getClient()->addListener($this->oAuth);
//        $groups->get($this->username);

        $orgs = Cache::remember(Auth::id().'-bitbucket-orgs', 15, function () {

            $rawTeams = new \Bitbucket\API\Teams();
            $rawTeams->getClient()->addListener($this->oAuth);
            $teams = json_decode($rawTeams->all('admin')->getContent(), true);

            // kinda crude, but we want to normalize our output for now
            foreach($teams['values'] as $team) {
                $org['name'] = $team['username'];
                $org['display_name'] = $team['display_name'];
                $org['link'] = $team['links']['html']['href'];
                $org['avatar_url'] = str_replace('32', '50', $team['links']['avatar']['href']);
                $org['role'] = 'admin';
                $orgs[] = $org;
            }

            return $orgs;

            // Leaving this in case I need it later for another request
            //print_r(json_decode($rawTeams->getClient()->getLastResponse()->getContent(), true));

        });

        return $orgs;

    }

    public function getOrgMembers($orgName)
    {
        $members = Cache::remember(Auth::id().'-bitbucket-orgMembers-'.$orgName, 15, function () use ($orgName) {

            $rawTeams = new \Bitbucket\API\Teams();
            $rawTeams->getClient()->addListener($this->oAuth);
            $teams = json_decode($rawTeams->members($orgName)->getContent(), true);

            foreach($teams['values'] as $team) {
                $member['login'] = $team['username'];
                $member['display_name'] = $team['display_name'];
                $member['link'] = $team['links']['html']['href'];
                $member['avatar_url'] = str_replace('32', '50', $team['links']['avatar']['href']);
                $members[] = $member;
            }

            return $members;

        });

        return $members;
    }

    public function addUserToOrg($org, $user)
    {
        try {
        $result = GitHub::organization()->members()->addMember($org, $user);
        } catch (RuntimeException $e) {
            echo $e->getCode();
        }

        return $result['state'];
    }

    public function removeUserFromOrg($org, $user)
    {
        $result = GitHub::organization()->members()->remove($org, $user);
    }

    public function getOrgRepos($orgName)
    {

        $repos = Cache::remember(Auth::id().'-bitbucket-orgRepos-'.$orgName, 15, function () use ($orgName) {

            $rawTeam = new \Bitbucket\API\Teams();
            $rawTeam->getClient()->addListener($this->oAuth);

            $page = new \Bitbucket\API\Http\Response\Pager($rawTeam->getClient(), $rawTeam->repositories($orgName));

            $rawRepos = json_decode($page->getCurrent()->getContent(), true);
            $totalRepos = $rawRepos['size'];

            do {
                if(!$rawRepos) {
                    $rawRepos = json_decode($page->fetchNext()->getContent(), true);
                   // dd($rawRepos);
                }
                foreach ($rawRepos['values'] as $thisRepo) {
                    $repo['name'] = $thisRepo['slug'];
                    $repo['display_name'] = $thisRepo['name'];
                    $repo['description'] = $thisRepo['description'];
                    $repo['link'] = $thisRepo['links']['html']['href'];
                    $repo['private'] = $thisRepo['is_private'];
                    $repo['updated'] = strtotime($thisRepo['updated_on']);
                    $repos[] = $repo;
                }
                $rawRepos = false;
            } while (count($repos) < $totalRepos);

            usort($repos, function ($repo1, $repo2) {
                if ($repo1['updated'] == $repo2['updated']) return 0;
                return $repo1['updated'] > $repo2['updated'] ? -1 : 1;
            });

            return $repos;

        });

        return $repos;

        //Debugger::$maxLength = 10000;
        //Debugger::barDump(print_r($repos, true));
    }

    public function getUserRepos()
    {
        $repos = Cache::remember(Auth::id().'-bitbucket-userRepos-'.$this->username, 15, function () {

            $bitbucket = new Repositories();
            $bitbucket->getClient()->addListener($this->oAuth);
    
            $rawRepos = json_decode( $bitbucket->all($this->username)->getContent(), true );

            foreach($rawRepos['values'] as $thisRepo) {
                $repo['name'] = $thisRepo['slug'];
                $repo['display_name'] = $thisRepo['name'];
                $repo['description'] = $thisRepo['description'];
                $repo['link'] = $thisRepo['links']['html']['href'];
                $repo['private'] = $thisRepo['is_private'];
                $repo['updated'] = strtotime($thisRepo['updated_on']);
                $repos[] = $repo;
            }

            usort($repos, function ($repo1, $repo2) {
                if ($repo1['updated'] == $repo2['updated']) return 0;
                return $repo1['updated'] > $repo2['updated'] ? -1 : 1;
            });

            return $repos;
    
            //Debugger::$maxLength = 10000;
            //Debugger::barDump(print_r($repos, true));
        });

        return $repos;
    }

    public function getRepoDetails($username, $repo)
    {
        // because BitBucket allows spaces in repo names
//        $repo = str_slug($repo);

        //$details = Cache::remember(Auth::id()."-bitbucket-repoDetails-{$username}-{$repo}", 15, function () use ($username, $repo) {

            $bitbucket = new \Bitbucket\API\Repositories\Repository();
            $bitbucket->getClient()->addListener($this->oAuth);

            $details = json_decode( $bitbucket->get($username, $repo)->getContent(), true );

            // do some normalizing
            $details['html_url'] = $details['links']['html']['href'];
            $details['created_at'] = $details['created_on'];
            $details['pushed_at'] = $details['updated_on'];

            return $details;

        //});

        return $details;
    }

    public function getRepoCommits($username, $repo, $timeFrame = 30)
    {
        // because BitBucket allows spaces in repo names
//        $repo = str_slug($repo);

        // initial retrieval for a repo won't have a start time
        if(is_null($timeFrame)) $timeFrame = 30;

        // we'll take a number of days or a date/timestamp
        if(is_int($timeFrame)) {
            $fromWhen = Carbon::now()->subdays($timeFrame);
        } else {
            $fromWhen = Carbon::parse($timeFrame);
        }

        $bitbucket = new \Bitbucket\API\Repositories\Commits();
        $bitbucket->getClient()->addListener($this->oAuth);

        $page = new \Bitbucket\API\Http\Response\Pager($bitbucket->getClient(), $bitbucket->all($username, $repo));

        $allCommits = [];
        $commitBatch = json_decode( $page->getCurrent()->getContent(), true );
        $fetchComplete = false;

        do {
            if(!$commitBatch) {
                $commitBatch = json_decode( $page->fetchNext()->getContent(), true );
            }
            foreach($commitBatch['values'] as $commit) {
                $commitTime = Carbon::parse($commit['date']);
                if($commitTime->lte($fromWhen)) {
                    $fetchComplete = true;
                    break;
                }
                $commit['sha'] = $commit['hash'];
                $commit['username'] = @$commit['author']['user']['username'];
                $commit['display_name'] = @$commit['author']['user']['display_name'];
                $commit['message'] = $commit['message'];
                $commit['commit_time'] = $commitTime->toDateTimeString();
                $allCommits[] = $commit;
            }
            $commitBatch = false;
            if(empty($allCommits)) $fetchComplete = true;
        }
        while (!$fetchComplete);

//        dd($allCommits);
        return $allCommits;
    }

    public function trackRepo($username, $repo, $repoHash)
    {
        $events = ['repo:push'];
        $params = [
            'url' => route('bitbucket.webhook', $repoHash),
            'description' => 'DevTeam Test Webhook',
            'active' => true,
            'events' => $events
        ];

        $bitbucket = new \Bitbucket\API\Repositories\Hooks();
        $bitbucket->getClient()->addListener($this->oAuth);

        $rawRepos = json_decode( $bitbucket->create($username, $repo, $params)->getContent(), true );
        return $rawRepos['uuid'];

    }

    private function getCurrentUser()
    {
        $this->user = GitHub::me()->show();
    }

    private function getOrgRoles()
    {
        if(empty($this->orgs)) return;

        foreach ($this->orgs as $key => $org) {
            $this->orgs[$key]['role'] = $this->getOrgRole($org['login']);
        }
    }

    private function getOrgRole($orgName)
    {
        $roleInfo = GitHub::organization()->members()->member($orgName,$this->user['login']);
        return $roleInfo['role'];
    }

    /**
     * @return string $token
     */
    private function validateAccessToken()
    {
        if (($this->user['tokenTime'] + $this->user['expiresIn'] - time()) < 60) {
            // refresh token
            $options = [
                'auth' => [config('services.bitbucket.client_id'), config('services.bitbucket.client_secret')],
                'form_params' => [
                    'grant_type' => 'refresh_token',
                    'refresh_token' => $this->user['refreshToken']
                ]
            ];

            $response = (new Guzzle)
                ->post('https://bitbucket.org/site/oauth2/access_token', $options)
                ->getBody()->getContents();

            $response = json_decode($response, true);

            // update the stored token and when we get it
            $this->user['token'] = $response['access_token'];
            $this->user['tokenTime'] = time();

            // store the updated provider details back to the user
            $service = ['bitbucket' => $this->user];
            $user = User::find(Auth::user()->id);
            $user->services = array_merge($user->services, $service);
            $user->save();
        }
        return $this->user['token'];
    }
}