<?php

namespace App\Modules;

use App\Repo;
use App\Commit;
use Carbon\Carbon;
use GrahamCampbell\GitHub\Facades\GitHub;

class CommitProcessor
{

    protected $github;

    public function loadGithubCommits(Repo $repo)
    {
        $this->github = new GitHubConnector;

        $commits = $this->github->getRepoCommits($repo->username, $repo->repo, $repo->last_commit);

        $count = 0;
        foreach($commits as $commit) {

            $inserted = Commit::create([
                'repo_id' => $repo->id,
                'sha' => $commit['sha'],
                'username' => $commit['author']['login'],
                'display_name' => $commit['commit']['author']['name'],
                'message' => $commit['commit']['message'],
                'commit_time' => Carbon::parse($commit['commit']['author']['date'])->toDateTimeString()
            ]);

            if($inserted) $count++;
        }

        // We set our last_commit to the timestamp of our FIRST received commit since GitHub
        // returns them in date-descending order.
        $repo->last_commit = Carbon::parse($commits[0]['commit']['author']['date'])->toDateTimeString();
        $repo->save();

        return $count;
    }

}