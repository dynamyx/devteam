<?php

namespace App\Modules;

use App\Repo;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use GrahamCampbell\GitHub\Facades\GitHub;
use Github\Exception\RuntimeException;
use function is_integer;
use Tracy\Debugger;

class GitHubConnector
{

    public $orgs = [];
    public $user = [];
    public $username = '';

    public function __construct($token)
    {
        GitHub::authenticate($token, null, 'http_token');

        $this->getCurrentUser();
        $this->username = $this->user['login'];
    }

    public function getOrgs()
    {

        $orgs = Cache::remember(Auth::id().'-github-orgs', 15, function () {

            $rawOrgs = GitHub::me()->organizations();
            $this->orgs = $rawOrgs;

            $this->getOrgRoles();

            foreach($this->orgs as $team) {
                $org['name'] = $team['login'];
                $org['display_name'] = $team['login'];
                $org['link'] = $team['url'];
                $org['avatar'] = $team['avatar_url'];
                $orgs[] = $org;
            }


            return $orgs;

        });

        return $orgs;

    }

    public function getOrgMembers($orgName)
    {
        $members = Cache::remember(Auth::id().'-github-orgMembers-'.$orgName, 15, function () use ($orgName) {

            $members = GitHub::organization()->members()->all($orgName);
            $admins = GitHub::organization()->members()->all($orgName, null, 'all', 'admin');

            foreach($members as $key => $member) {
                $members[$key]['org_admin'] = false;
                if(in_array($member, $admins)) {
                    $members[$key]['org_admin'] = true;
                }
                $members[$key]['link'] = $members[$key]['html_url'];
                $members[$key]['display_name'] = $members[$key]['login'];
            }
            return $members;

        });

        return $members;
    }

    public function addUserToOrg($org, $user)
    {
        try {
        $result = GitHub::organization()->members()->addMember($org, $user);
        } catch (RuntimeException $e) {
            echo $e->getCode();
        }

        return $result['state'];
    }

    public function removeUserFromOrg($org, $user)
    {
        $result = GitHub::organization()->members()->remove($org, $user);
    }

    public function getOrgRepos($orgName)
    {

        $repos = Cache::remember(Auth::id().'-github-orgRepos-'.$orgName, 15, function () use ($orgName) {

            $rawRepos =  GitHub::organization()->repositories($orgName);

            foreach ($rawRepos as $thisRepo) {
                $repo['name'] = $thisRepo['name'];
                $repo['display_name'] = $thisRepo['name'];
                $repo['description'] = $thisRepo['description'];
                $repo['link'] = $thisRepo['url'];
                $repo['private'] = $thisRepo['private'];
                $repo['updated'] = strtotime($thisRepo['pushed_at']);
                $repos[] = $repo;
            }

            usort($repos, function ($repo1, $repo2) {
                if ($repo1['updated'] == $repo2['updated']) return 0;
                return $repo1['updated'] > $repo2['updated'] ? -1 : 1;
            });

            return $repos;


        });

        return $repos;

        //Debugger::$maxLength = 10000;
        //Debugger::barDump(print_r($repos, true));
    }

    public function getUserRepos()
    {
        $repos = Cache::remember(Auth::id().'-github-userRepos-'.$this->username, 15, function () {

            $rawRepos = GitHub::user()->myRepositories(['type' => 'owner']);

            foreach ($rawRepos as $thisRepo) {
                $repo['name'] = $thisRepo['name'];
                $repo['display_name'] = $thisRepo['name'];
                $repo['description'] = $thisRepo['description'];
                $repo['link'] = $thisRepo['url'];
                $repo['private'] = $thisRepo['private'];
                $repo['updated'] = strtotime($thisRepo['pushed_at']);
                $repos[] = $repo;
            }

            usort($repos, function ($repo1, $repo2) {
                if ($repo1['updated'] == $repo2['updated']) return 0;
                return $repo1['updated'] > $repo2['updated'] ? -1 : 1;
            });

            return $repos;
            //Debugger::$maxLength = 10000;
            //Debugger::barDump(print_r($repos, true));
        });

        return $repos;
    }

    public function getRepoDetails($username, $repo)
    {
        $details = Cache::remember(Auth::id()."-github-repoDetails-{$username}-{$repo}", 15, function () use ($username, $repo) {

            return GitHub::repo()->show($username, $repo);

        });

        return $details;
    }

    public function getRepoCommits($username, $repo, $timeFrame = 30)
    {
        // initial retrieval for a repo won't have a start time
        if(is_null($timeFrame)) $timeFrame = 30;

        // we'll take a number of days or a date/timestamp
        if(is_int($timeFrame)) {
            $fromWhen = Carbon::now()->subdays($timeFrame)->toIso8601String();
        } else {
            $fromWhen = Carbon::parse($timeFrame)->toIso8601String();
        }

        $rawCommits = $allCommits = [];
        $page = 1;
        do {
            $commitBatch = GitHub::repository()->commits()->all($username, $repo, ['since' => $fromWhen, 'per_page' => 100, 'page' => $page]);
            $page++;
            $rawCommits = array_merge($rawCommits, $commitBatch);
        }
        while (count($commitBatch) == 100);

        // apply some normalization
        foreach($rawCommits as $commit) {
            $thisCommit['sha'] = $commit['sha'];
            $thisCommit['username'] = $commit['author']['login'];
            $thisCommit['display_name'] = $commit['commit']['author']['name'];
            $thisCommit['message'] = $commit['commit']['message'];
            $thisCommit['commit_time'] = Carbon::parse($commit['commit']['author']['date'])->toDateTimeString();
            $allCommits[] = $thisCommit;
        }

        return $allCommits;
    }

    private function getCurrentUser()
    {
        $this->user = GitHub::me()->show();
        $this->user['provider'] = 'github';
    }

    private function getOrgRoles()
    {
        if(empty($this->orgs)) return;

        foreach ($this->orgs as $key => $org) {
            $this->orgs[$key]['role'] = $this->getOrgRole($org['login']);
        }
    }

    private function getOrgRole($orgName)
    {
        $roleInfo = GitHub::organization()->members()->member($orgName,$this->user['login']);
        return $roleInfo['role'];
    }
}