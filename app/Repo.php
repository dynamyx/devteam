<?php

namespace App;

use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Model;
use function is_null;

class Repo extends Model
{
    protected $fillable = [
        'user_id',
        'source',
        'username',
        'repo'
    ];

    public function commits()
    {
        return $this->hasMany('App\Commit', 'repo');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public static function getUserRepos($user_id = null)
    {
        if(is_null($user_id)) {
            $user_id = Auth::id();
        }

        $trackedRepos = static::where('user_id', $user_id)
                              ->where('active', true)
                              ->get();

        return $trackedRepos;
    }

    public static function buildTrackedList($user_id = null)
    {
        $trackedRepos = static::getUserRepos($user_id);

        $trackedList = [];
        foreach($trackedRepos as $repo) {
            $trackedList[$repo->source][$repo->username][] = $repo->repo;
        }

        return $trackedList;
    }

}
