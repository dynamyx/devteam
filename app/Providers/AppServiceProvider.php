<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Collective\Html\FormBuilder as Form;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Form::component('bsText', 'components.bstext', ['label', 'name', 'value' => null, 'attributes' => []]);
        Form::component('bsCheck', 'components.bscheck', ['label', 'name', 'value' => false, 'attributes' => []]);

        $value = \App\Utilities\Copyright::displayNotice();

        view()->share('copyright', $value);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
