<?php

namespace App;

use App\Repo;
use App\Modules\GitHubConnector;
use Illuminate\Database\Eloquent\Model;

class Commit extends Model
{
    protected $fillable = [
        'repo_id',
        'sha',
        'username',
        'display_name',
        'message',
        'commit_time'
    ];

    public $timestamps = false;

    public function repo()
    {
        return $this->belongsTo('App\Repo');
    }
}
