<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Redirect;

class SettingsController extends Controller
{

    public function __construct()
    {

        $this->middleware('auth');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */

    public function edit()
    {

        $id = Auth::user()->id;

        $user = User::findOrFail($id);

        return view('settings.edit', compact('user'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */

    public function update(Request $request)
    {

        $id = Auth::user()->id;

        $this->validate($request, [
            'first_name' => 'required|max:20',
            'last_name' => 'required|max:20',
            'email' => 'required|email|max:255|unique:users,email,' .$id,
            'is_subscribed' => 'boolean'
        ]);

        $user = User::findOrFail($id);
//        $services = $request->services;
        $user->update([
            'first_name'  => $request->first_name,
            'last_name'  => $request->last_name,
            'email' => $request->email,
            'is_subscribed' => $request->is_subscribed,
            'services' => $request->services,
        ]);

        return redirect()->action('SettingsController@edit', [$user]);

    }

    /**
     * Generate a new user API key
     *
     * @param  Request  $request
     * @param  int  $id
     * @return Response
     */

    public function generateKey(Request $request)
    {

        $id = Auth::user()->id;

        $user = User::findOrFail($id);

        if($request->reset_access_key) {
            //$updateArray['access_key'] = sha1($user->email.Carbon::now()->format('Y-m-d H:i:s'));
            $user->generateAPIKey();
        }

        return redirect()->action('SettingsController@edit', [$user]);

    }
}