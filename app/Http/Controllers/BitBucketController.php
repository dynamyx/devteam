<?php

namespace App\Http\Controllers;

use App\Repo;
use App\Commit;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Modules\BitBucketConnector;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Event;

class BitBucketController extends Controller
{

    public function __construct()
    {

    }

    protected function bitbucket()
    {
        return new BitBucketConnector();
        return new BitBucketConnector(Auth::user()->services['bitbucket']);
    }
    /**
     * Show the list of organizations
     *
     * @return \Illuminate\Http\Response
     */
    public function organizations()
    {
        $user = $this->bitbucket()->user;
        $orgs = $this->bitbucket()->getOrgs();

        return view('module.github.organizations', compact('user', 'orgs'));
    }

    public function organization($org)
    {
        //$members = $this->bitbucket()->getOrgMembers($org);

//        $repos = $this->bitbucket()->getOrgRepos($org);
//        dd($repos);

        return view('module.github.org_details', ['user' => $this->bitbucket()->user, 'org' => $org]);
    }

    public function displayRepoDetails($username, $repo)
    {
        $repo = $this->bitbucket()->getRepoDetails($username, $repo);
        $repo['name'] = ($repo['name'] == $repo['slug']) ? $repo['name'] : $repo['slug'];

        $tracked = Repo::buildTrackedList();

        if(isset($tracked['bitbucket'][$username]) && in_array($repo['name'], $tracked['bitbucket'][$username])) {
            $repo['is_tracked'] = true;
            $trackedRepo = Repo::where('user_id', Auth::id())
                               ->where('source', 'bitbucket')
                               ->where('username', $username)
                               ->where('repo', $repo['name'])
                               ->first();

            $commits = Commit::where('repo_id', $trackedRepo->id)->orderBy('commit_time', 'desc')->get();
        } else {
            $repo['is_tracked'] = false;
            $fromTime = Carbon::parse($repo['pushed_at'])->subdays(30)->toDayDateTimeString();
            $commits = $this->bitbucket()->getRepoCommits($username, $repo['name'], $fromTime);
        }

        return view('module.github.repo_details', ['user' => $this->bitbucket()->user, 'repo' => $repo, 'commits' => $commits]);
    }

    public function APIuserRepos()
    {
        $name = $this->bitbucket()->username;
        $repos = $this->bitbucket()->getUserRepos();

        $tracked = Repo::buildTrackedList();

        foreach($repos as $key => $repo) {
            if(isset($tracked['bitbucket'][$name]) && in_array($repo['name'], $tracked['bitbucket'][$name])) {
                $repos[$key]['tracked'] = true;
            } else {
                $repos[$key]['tracked'] = false;
            }
        }

        return $repos;
    }

   public function APIorgRepos($org)
    {
        $repos = $this->bitbucket()->getOrgRepos($org);

        $tracked = Repo::buildTrackedList();

        foreach($repos as $key => $repo) {
            if(isset($tracked['bitbucket'][$org]) && in_array($repo['name'], $tracked['bitbucket'][$org])) {
                $repos[$key]['tracked'] = true;
            } else {
                $repos[$key]['tracked'] = false;
            }
        }

        return $repos;
    }

    public function APIorgMembers($org)
    {
        return $this->bitbucket()->getOrgMembers($org);
    }

    public function addOrgUser(Request $request)
    {
        $user = $request->user;
        $org = $request->org;

        try {
            $result = $this->bitbucket()->addUserToOrg($org, $user);
        }

        catch (RuntimeException $e) {
            echo $e->getCode();
        }
//        dd($result);

        return redirect('/github/organization/'.$org);
    }

    public function removeOrgUser($org, $user)
    {
        $result = $this->bitbucket()->removeUserFromOrg($org, $user);

        return redirect('/github/organization/'.$org);
    }

    public function getRepoCommits($username, $repo)
    {
        $commits = $this->bitbucket()->getRepoCommits($username, $repo);

        return view('module.github.repo_commits', ['user' => $username, 'repo' => $repo, 'commits' => $commits]);
    }
}
