<?php

namespace App\Http\Controllers;

use App\Commit;
use App\Repo;
use Carbon\Carbon;
use Illuminate\Support\Facades\Log;
use Illuminate\Http\Request;
use Vinkla\Hashids\Facades\Hashids;

class WebHookController extends Controller
{
    public function BitBucketPush(Request $request)
    {
        $repo_id = Hashids::decode($request->repo_hash);
        $webhookId = trim($request->header('X-Hook-UUID'),'{}');
        $payloadType = $request->header('X-Event-Key');
        if($payloadType !== 'repo:push') {
            return 'Ok';
        }

        Log::info('Repo: '.$repo_id[0]);
        Log::info($request->push);

        $repo = Repo::whereId($repo_id[0])->whereWebhookId($webhookId)->first();

        $push = $request->push['changes'][0];

        if($push['truncated']) {
            // request commits manually....queue?
            // we'll go loop through commits and store all we don't already have.
        }

        $commits = $push['commits'];
        foreach($commits as $commit) {
            Commit::create([
                'repo_id' => $repo->id,
                'sha' => $commit['hash'],
                'username' => $commit['author']['user']['username'],
                'display_name' => $commit['author']['user']['display_name'],
                'message' => $commit['message'],
                'commit_time' => Carbon::parse($commit['date']),
            ]);
        }


        return 'Ok';
    }
}
