<?php

namespace App\Http\Controllers;

use App\Repo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Modules\BitBucketConnector;
use Vinkla\Hashids\Facades\Hashids;

class RepoController extends Controller
{

    protected function bitbucket()
    {
        return new BitBucketConnector();
    }

    public function trackRepo(Request $request)
    {

        $repo = Repo::create([
            'user_id' => Auth::id(),
            'source' => $request->source,
            'username' => $request->username,
            'repo' => $request->repo,
        ]);

        // TODO: Add call to create webhook at respective service
        $repoHash = Hashids::encode($repo->id);
        if($request->source == 'bitbucket') {
            $webhookId = $this->bitbucket()->trackRepo($request->username, $request->repo, $repoHash);
        }

        // for some reason, BitBucket wraps the UUID in curly braces ??
        $repo->webhook_id = trim($webhookId,'{}');
        $repo->save();

        return "Tracked";

    }

    public function untrackRepo(Request $request)
    {

    }
}
