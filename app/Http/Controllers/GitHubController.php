<?php

namespace App\Http\Controllers;

use App\Repo;
use App\Commit;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Modules\GitHubConnector;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Event;

class GitHubController extends Controller
{

    public function __construct()
    {

    }

    protected function github()
    {
        return new GitHubConnector(Auth::user()->services['github']['token']);
    }
    /**
     * Show the list of organizations
     *
     * @return \Illuminate\Http\Response
     */
    public function organizations()
    {
        $user = $this->github()->user;
        $orgs = $this->github()->getOrgs();

        return view('module.github.organizations', compact('user', 'orgs'));
    }

    public function organization($org)
    {
        // Get all an organization's members
        //$gh = GitHub::organization()->members()->all('Get-Altitude', null, 'all', 'admin');

        //$gh = GitHub::organization()->members()->member('Get-Altitude');
        //$gh = GitHub::me()->organizations();
        //$gh = GitHub::me()->show();

        //$members = $this->github()->getOrgMembers($org);

        //$repos = $this->github()->getOrgRepos($org);
        //dd($members);

        return view('module.github.org_details', ['user' => $this->github()->user, 'org' => $org]);
    }

    public function displayRepoDetails($username, $repo)
    {
        $repo = $this->github()->getRepoDetails($username, $repo);

        $tracked = Repo::buildTrackedList();

        if(isset($tracked['github'][$username]) && in_array($repo['name'], $tracked['github'][$username])) {
            $repo['is_tracked'] = true;
            $trackedRepo = Repo::where('user_id', Auth::id())
                               ->where('source', 'github')
                               ->where('username', $username)
                               ->where('repo', $repo['name'])
                               ->first();

            $commits = Commit::where('repo_id', $trackedRepo->id)->orderBy('commit_time', 'desc')->get();
        } else {
            $repo['is_tracked'] = false;
            $fromTime = Carbon::parse($repo['pushed_at'])->subdays(30)->toDayDateTimeString();
            $commits = $this->github()->getRepoCommits($username, $repo['name'], $fromTime);
        }

        return view('module.github.repo_details', ['user' => $this->github()->user, 'repo' => $repo, 'commits' => $commits]);
    }

    public function APIuserRepos()
    {
        $name = $this->github()->username;
        $repos = $this->github()->getUserRepos();

        $tracked = Repo::buildTrackedList();

        foreach($repos as $key => $repo) {
            if(isset($tracked['github'][$name]) && in_array($repo['name'], $tracked['github'][$name])) {
                $repos[$key]['tracked'] = true;
            } else {
                $repos[$key]['tracked'] = false;
            }
        }

        return $repos;
    }

    public function APIorgRepos($org)
    {
        $repos = $this->github()->getOrgRepos($org);

        $tracked = Repo::buildTrackedList();

        foreach($repos as $key => $repo) {
            if(isset($tracked['github'][$org]) && in_array($repo['name'], $tracked['github'][$org])) {
                $repos[$key]['tracked'] = true;
            } else {
                $repos[$key]['tracked'] = false;
            }
        }

        return $repos;
    }

    public function APIorgMembers($org)
    {
        return $this->github()->getOrgMembers($org);
    }

    public function addOrgUser(Request $request)
    {
        $user = $request->user;
        $org = $request->org;

        try {
            $result = $this->github()->addUserToOrg($org, $user);
        }

        catch (RuntimeException $e) {
            echo $e->getCode();
        }
//        dd($result);

        return redirect('/github/organization/'.$org);
    }

    public function removeOrgUser($org, $user)
    {
        $result = $this->github()->removeUserFromOrg($org, $user);

        return redirect('/github/organization/'.$org);
    }

    public function getRepoCommits($username, $repo)
    {
        $commits = $this->github()->getRepoCommits($username, $repo);

        return view('module.github.repo_commits', ['user' => $username, 'repo' => $repo, 'commits' => $commits]);
    }
}
