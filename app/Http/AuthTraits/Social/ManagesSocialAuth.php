<?php

namespace App\Http\AuthTraits\Social;

use App\User;
use App\Exceptions\EmailNotProvidedException;
use Redirect;
use Socialite;

trait ManagesSocialAuth
{
    // the traits contain the methods needed for the handleProviderCallback

    use FindsOrCreatesUsers,
        RoutesSocialUser,
        SetsAuthUser,
        SyncsSocialUsers,
        VerifiesSocialUsers;

    private $userName;

    private $provider;

    private $approvedProviders = [ 'facebook', 'github', 'bitbucket'];

    public function handleProviderCallback($provider)
    {

        $this->verifyProvider($this->provider = $provider);

        $socialUser = $this->getUserFromSocialite($provider);

        $providerEmail = $socialUser->getEmail();

        if ($this->socialUserHasNoEmail($providerEmail)) {

            throw new EmailNotProvidedException;

        }

        $this->setSocialUserName($socialUser);

        if ($this->socialUserAlreadyLoggedIn()) {

            $this->checkIfAccountSyncedOrSync($socialUser);

        }

        // set authUser from socialUser

        $authUser = $this->setAuthUser($socialUser);

        $this->loginAuthUser($authUser);

        $this->storeProviderDetails($socialUser, $authUser);

        $this->logoutIfUserNotActiveStatus();

        return $this->redirectUser();

    }

    protected function storeProviderDetails($socialUser, $authUser)
    {
        // store the time we retrieve the token as some providers need periodic refreshing
        $socialUser->tokenTime = time();

        $service = [$this->provider => $socialUser];
        $user = User::find($authUser->id);
        if(is_array($user->services)) {
            $user->services = array_merge($user->services, $service);
        } else {
            $user->services = $service;
        }
        $user->save();
    }
}