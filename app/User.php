<?php

namespace App;

use Carbon\Carbon;
use Vinkla\Hashids\Facades\Hashids;
use App\Http\AuthTraits\OwnsRecord;
use App\Traits\HasModelTrait;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\UserRequest;

class User extends Authenticatable
{
    use Notifiable, OwnsRecord, HasModelTrait, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['first_name',
                           'last_name',
                           'email',
                           'company',
                           'is_subscribed',
                           'is_admin',
                           'status_id',
                           'access_key',
                           'services',
                           'password'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */protected $casts = [
        'services' => 'array',
    ];

    public function updateUser($user, UserRequest $request)
    {

        return  $user->update(['first_name'  => $request->first_name,
                               'last_name'  => $request->last_name,
                               'email' => $request->email,
                               'is_subscribed' => $request->is_subscribed,
                               'is_admin' => $request->is_admin,
                               'status_id' => $request->status_id,
                               'services' => $request->services,
        ]);


    }

    public function generateAPIKey() {
        return $this->update(['access_key' => Hashids::connection('account')->encode($this->id . time())]);
    }

    public function getNameAttribute()
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function showAdminStatusOf($user)
    {

        return $user->is_admin ? 'Yes' : 'No';

    }

    public function showNewsletterStatusOf($user)
    {

        return $user->is_subscribed == 1 ? 'Yes' : 'No';

    }

    public function isAdmin()
    {

        return Auth::user()->is_admin == 1;
    }

    public function isActiveStatus()
    {

        return Auth::user()->status_id == 10;
    }

    public function widgets()
    {

        return $this->hasMany('App\Widget');
    }

    public function socialProviders()
    {

        return $this->hasMany('App\SocialProvider');

    }

    public function profile()
    {

        return $this->hasOne('App\Profile');
    }

    public function messages()
    {

        return $this->hasMany(Message::class);

    }
}
