<footer class="main-footer">
    <div class="container">

        <strong>Copyright &copy; {{ date('Y') }} <a href="/">Dev Team Manager and Web Dynamyx LLC</a>.</strong> All rights reserved.

        <div class="pull-right">
            <a href="/privacy">Site Privacy Policy</a> | <a href="/terms">Terms of Use</a> | <a href="/contact">Contact Us</a>
        </div>

    </div>
    <!-- /.container -->
</footer>