@extends('layouts.master-admin')

@section('title')

    <title>Repository Details</title>

@endsection

@section('content')

    <!-- content-wrapper -->

    <div class="content-wrapper">

        <!-- container -->

        <div class="xcontainer">

            <!-- content-header has breadcrumbs -->

            <section class="content-header">


                <ol class="breadcrumb">

                    <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li><a href="/{!! $user['provider'] !!}/organization">Organization Repos</a></li>
                    <li class="active">Repository Details</li>

                </ol>

            </section>

            <!-- end content-header section -->

            <!-- content -->

            <section class="content">


                <h3>Details for <strong>{{ $repo['full_name'] }}</strong></h3>

                <h4>Repository Page: <a href="{!! $repo['html_url'] !!}" target="_blank">{!! $repo['html_url'] !!}</a></h4>

                <table class="table table-striped table-condensed col-lg-4 col-sm-6">
                    <tr>
                        <td>Created</td>
                        <th>{!! Carbon\Carbon::parse($repo['created_at'])->toDayDateTimeString() !!}</th>
                    </tr>
                    <tr>
                        <td>Last Push</td>
                        <th>{!! Carbon\Carbon::parse($repo['pushed_at'])->toDayDateTimeString() !!}</th>
                    </tr>
                </table>

                <table class="table table-striped table-condensed table-hover">
                    <tr>
                        <th>Commit Time</th>
                        <th>Author</th>
                        <th>Message</th>
                    </tr>
                    @foreach($commits as $commit)
                        <tr>
                        @if($repo['is_tracked'])
                            <td>{!! $commit->commit_time !!}</td>
                            <td>{!! $commit->display_name !!} ({!! $commit->username !!})</td>
                            <td>{!! $commit->message !!}</td>
                        @else
                            <td>{!! $commit['commit_time'] !!}</td>
                            <td>{!! $commit['display_name'] !!} ({!! $commit['username'] !!})</td>
                            <td>{!! $commit['message'] !!}</td>
                        @endif
                        </tr>
                    @endforeach
                </table>
                <div class="row">
                    <div class="col-sm-6">

                    </div>
                </div>

                {{--
                                <table class="table table-striped table-condensed">
                                    <tr>
                                        <th>Key</th>
                                        <th>Value</th>
                                    </tr>
                                @foreach($repo as $key => $value)
                                    <tr>
                                        <td>{!! $key !!}</td>
                                        @if(is_array($value))
                                            <td>
                                                <table class="table table-striped table-condensed">
                                                @foreach($value as $innerkey => $innervalue)
                                                    @if(is_array($innervalue))
                                                        @continue
                                                    @endif
                                                    <tr>
                                                        <td>{!! $innerkey !!}</td>
                                                        <td>{!! $innervalue !!}</td>
                                                    </tr>
                                                @endforeach
                                                </table>
                                            </td>
                                        @else
                                            <td>{!! $value !!}</td>
                                        @endif
                                    </tr>
                                @endforeach
                                </table>
                --}}

            </section>

            <!-- end content section -->

        </div>

        <!-- end container -->

    </div>

    <!-- end content-wrapper -->

@stop