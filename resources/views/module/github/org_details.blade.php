@extends('layouts.master-admin')

@section('title')

    <title>Org/Team Details</title>

@endsection

@section('content')

    <!-- content-wrapper -->

    <div class="content-wrapper">

        <!-- container -->

        <div class="container">

            <!-- content-header has breadcrumbs -->

            <section class="content-header">


                <ol class="breadcrumb">

                    <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li><a href="/{!! $user['provider'] !!}/organization">Org/Team List</a></li>
                    <li class="active">Org/Team Details</li>

                </ol>

            </section>

            <!-- end content-header section -->

            <!-- content -->

            <section class="content">

                <org-users provider="{!! $user['provider'] !!}" orgname="{!! $org !!}"></org-users>

                <div class="row">
                    {!! Form::open(array('url' => $user['provider'] . '/organization/add')) !!}

                    {!! Form::hidden('org', $org) !!}
                    {!! Form::bsText('GitHub Username', 'user') !!}
                    {!! Form::submit('Add User' , array('class' => 'btn btn-primary')) !!}

                    {!! Form::close() !!}
                </div>

                <org-repos provider="{!! $user['provider'] !!}" orgname="{!! $org !!}"></org-repos>

            </section>

            <!-- end content section -->

        </div>

        <!-- end container -->

    </div>

    <!-- end content-wrapper -->

@stop