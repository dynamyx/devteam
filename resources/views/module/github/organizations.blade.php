@extends('layouts.master-admin')

@section('title')

    <title>Orgs / Teams</title>

@endsection

@section('content')

    <!-- content-wrapper -->

    <div class="content-wrapper">

        <!-- container -->

        <div class="container">

            <!-- content-header has breadcrumbs -->

            <section class="content-header">


                <ol class="breadcrumb">

                    <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li class="active"><a href="/{!! $user['provider'] !!}/organization">Org/Team List</a></li>

                </ol>

            </section>

            <!-- end content-header section -->

            <!-- content -->

            <section class="content">

                <h2>Orgs/Teams for {!! $user['name'] !!}</h2>
                @foreach($orgs as $org)

                    <div class="row">
                        <div class="col-sm-6">
                            <p>Organization: <a href="/{!! $user['provider'] !!}/organization/{{ $org['name'] }}">{{ $org['name'] }}</a></p>
                        </div>
                    </div>

                @endforeach

                <h2>Direct Repositories</h2>

                <user-repos provider="{!! $user['provider'] !!}" username="{!! $user['login'] !!}"></user-repos>

            </section>

            <!-- end content section -->

        </div>

        <!-- end container -->

    </div>

    <!-- end content-wrapper -->

@endsection
