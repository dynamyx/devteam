@extends('layouts.master-admin')

@section('title')

    <title>Repo Commits</title>

@endsection

@section('content')

    <!-- content-wrapper -->

    <div class="content-wrapper">

        <!-- container -->

        <div class="container">

            <!-- content-header has breadcrumbs -->

            <section class="content-header">


                <ol class="breadcrumb">

                    <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li><a href="/github/organization">Organization List</a></li>
                    <li class="active">Organization Members</li>

                </ol>

            </section>

            <!-- end content-header section -->

            <!-- content -->

            <section class="content">

                <h2>Commits for {!! $user !!}/{!! $repo !!}</h2>

                @foreach($commits as $commit)
                    <div style="margin-bottom: 8px;">
                        {!! $commit['author']['login'] !!} - {!! $commit['commit']['author']['name'] !!} - {!! $commit['commit']['author']['date'] !!}<br />
                        {!! $commit['commit']['message'] !!}
                    </div>
                @endforeach

            </section>

            <!-- end content section -->

        </div>

        <!-- end container -->

    </div>

    <!-- end content-wrapper -->

@stop