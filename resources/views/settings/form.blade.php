<form class="form form-border" role="form" method="POST" action="{{ url('/settings') }}">

      {{ csrf_field() }}

    <!-- user name input -->

    <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">

         <label class="control-label">First Name</label>

         <input type="text" class="form-control" name="first_name" value="{{ $user->first_name }}">

        @if ($errors->has('first_name'))
            <span class="help-block">
                  <strong>{{ $errors->first('first_name') }}</strong>
            </span>
        @endif

    </div>

    <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">

         <label class="control-label">Last Name</label>

         <input type="text" class="form-control" name="last_name" value="{{ $user->last_name }}">

        @if ($errors->has('last_name'))
            <span class="help-block">
                  <strong>{{ $errors->first('last_name') }}</strong>
            </span>
        @endif

    </div>

    <!-- end user name input -->

    <!-- email input -->

    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">

        <label for="email" class="control-label">E-Mail Address</label>

            <input id="email" type="email" class="form-control" name="email" value="{{ $user->email }}">

            @if ($errors->has('email'))
                <span class="help-block">
                      <strong>{{ $errors->first('email') }}</strong>
                </span>
            @endif

    </div>

    <!-- end email input -->

    <!-- is_subscribed select -->

    <div class="form-group{{ $errors->has('is_subscribed') ? ' has-error' : '' }}">

        <label class="control-label">Is Subscribed?</label>

        <select class="form-control" id="is_subscribed" name="is_subscribed">
            <option value="{{ $user->is_subscribed }}">
                           {{ $user->is_subscribed == 1 ? 'Yes' : 'No' }}</option>
            <option value="1">Yes</option>
            <option value="0">No</option>
        </select>


        @if ($errors->has('is_subscribed'))
            <span class="help-block">
                  <strong>{{ $errors->first('is_subscribed') }}</strong>
            </span>
        @endif

    </div>

    <!-- end is_subscribed select -->

      {!! Form::bsText('GitHub Token', 'services[github]', $user->services['github']) !!}
      {!! Form::bsText('BitBucket Token', 'services[bitbucket]', $user->services['bitbucket']) !!}

      <!-- submit button -->

    <div class="form-group">

        <button type="submit" class="btn btn-primary btn-lg">Update</button>

    </div>

    <!-- end submit button -->

</form>

<br />

<form class="form form-border" role="form">

    <!-- reset password button -->

    <div class="form-group">

        <a href="{{ url('/password/reset') }}">

            <button type="button" class="btn btn-lg btn-warning">Reset Password</button>

        </a>

    </div>

    <!-- end reset password button -->

</form>

<br /><br/>

<form class="form form-border" role="form" method="POST" action="{{ url('/settings/generate-key') }}">

    {{ csrf_field() }}

    <div class="callout callout-danger">
        <h4><u>ONLY</u> reset your API Key if you need to generate all new embed codes for every feature on every page!</h4>
    </div>

    <!-- access_key input -->

    <div class="form-group">

        <label for="email" class="control-label">Current API Access Key</label>

        <input id="access_key" type="text" class="form-control" name="access_key" value="{{ $user->access_key }}" disabled="disabled">

    </div>

    <!-- end access_key input -->

    <!-- reset access_key input -->

    <div class="form-group form-inline">

        <label for="reset_access_key" class="control-label">
            <input id="reset_access_key" type="checkbox" class="form-inline" name="reset_access_key" value="1"> Generate New Access Key
        </label>

    </div>

    <!-- end reset access_key input -->

    <!-- submit button -->

    <div class="form-group">

        <button type="submit" class="btn btn-danger btn-lg">Reset API Key!</button>

    </div>

    <!-- end submit button -->

</form>