<div class="form-group{{ $errors->has($name) ? ' has-error' : '' }}">
    <label>
        {{ Form::checkbox($name, $value) }}
        {{ $label }}
    </label>
</div>
