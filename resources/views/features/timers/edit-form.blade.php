@if($timer->id)
    {!! Form::model($timer, ['route' => ['timer.update', $timer], 'method' => 'patch', 'class' => 'form form-border']) !!}
@else
    {!! Form::open(['route' => 'timer.store', 'class' => 'form form-border', 'role' => 'form']) !!}
@endif

    {!! Form::bsText('Timer Name', 'name', $timer->name) !!}

    {!! Form::bsText('Default End Time -- Format: YYYY-MM-DD HH:MM:SS', 'default_endtime', $timer->default_endtime) !!}

    {!! Form::bsText('Default Duration', 'default_duration', $timer->default_duration) !!}

    {!! Form::bsCheck('Automatically Add Contact Timer Without HTTP_Post?', 'can_auto_create', $timer->can_auto_create) !!}

    {!! Form::bsCheck('Redirect After Expiration?', 'expire_redirect', $timer->expire_redirect) !!}

    {!! Form::bsText('Expired Redirect URL', 'expire_redirect_url', $timer->expire_redirect_url) !!}

    {!! Form::bsCheck('Redirect If No ContactID Present?', 'empty_redirect', $timer->empty_redirect) !!}

    {!! Form::bsText('No Contact Redirect URL', 'empty_redirect_url', $timer->empty_redirect_url) !!}

    {!! Form::bsCheck('Use Only On Specific Pages?', 'is_page_specific', $timer->is_page_specific) !!}


    <div class="form-group">

    @if($timer->id)
        <button type="submit" class="btn btn-primary btn-lg">Update Timer</button>
    @else
        <button type="submit" class="btn btn-primary btn-lg">Add Timer</button>
    @endif

    </div>

</form>