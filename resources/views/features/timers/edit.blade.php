@extends('layouts.master-admin')

@section('title')

    <title>Edit {{ $timer->name }}</title>

@endsection

@section('content')

    <!-- content-wrapper -->

    <div class="content-wrapper">

        <div class="container">


            <!-- content-header has breadcrumbs -->

            <section class="content-header">


                <ol class="breadcrumb">

                    <li><a href="/"><i class="fa fa-dashboard"></i> Home</a></li>
                    <li><a href="/timer">Timers</a></li>
                    <li class="active">Edit Timer</li>

                </ol>

            </section>

            <!-- content -->

            <section class="content">

                <div class="col-xs-8 col-md-6">

                @if($timer->id )
                    <h2 class="min-width-200">Edit Timer: <strong>{{ $timer->name }}</strong></h2>
                @else
                    <h2 class="min-width-200">Add Timer:</h2>
                @endif

                    <!-- edit form -->

                    @include('features.timers.edit-form')

                    <!-- end edit form -->

                </div>

                <div class="col-md-6">

                @if($timer->id)
                    <h2 class="min-width-200">Timer Stats:</h2>

                    <h3>Contacts Registered for Timer: <strong>{{ $timer->contactTimers()->count() }}</strong></h3>

                    <h3>Total Views Recorded: <strong>{{ $timer->timerLogCount() }}</strong></h3>

                    <h3>Retrieve Time URL:</h3>
                    <input class="form-control" value="{{ url(Auth::user()->access_key .'/timer/'. $timer->getTimerHash() .'/~ContactId~') }}" />

                    <h3>Set Timer for Contact HTTP_Post URL:</h3>
                    <input class="form-control" value="{{ url(Auth::user()->access_key .'/timer') }}" />
                    <h4>With the following attributes passed in:</h4>
                    <code>
                        timer = {{ $timer->getTimerHash() }}<br />
                        contact = ~Contact.Id~<br />
                    </code>
                @endif

                </div>

            </section>

            <!-- end content section -->

        </div>

        <!-- end container -->

    </div>

    <!-- end content-wrapper -->

@endsection