Vue.component('data-extracted-sample', require('./components/DataExtractedSample.vue'));
Vue.component('example', require('./components/Example.vue'));
Vue.component('marketing-image-grid', require('./components/MarketingImageGrid.vue'));
Vue.component('profile-grid', require('./components/ProfileGrid.vue'));
Vue.component('user-grid', require('./components/UserGrid.vue'));
Vue.component('widget-grid', require('./components/WidgetGrid.vue'));
Vue.component('breadcrumbs', require('./components/Breadcrumbs.vue'));

Vue.component('user-repos', require('./components/UserRepos.vue'));
Vue.component('org-repos', require('./components/OrgRepos.vue'));
Vue.component('org-users', require('./components/OrgUsers.vue'));