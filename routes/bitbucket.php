<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| BitBucket Control Routes
|--------------------------------------------------------------------------
*/

Route::get('organization', 'BitBucketController@organizations');

Route::get('organization/{org}', 'BitBucketController@organization');
Route::post('organization/add', 'BitBucketController@addOrgUser');
Route::get('organization/{org}/remove/{user}', 'BitBucketController@removeOrgUser');

Route::get('repository/{username}/{repo}/details', 'BitBucketController@displayRepoDetails');
Route::get('repository/{username}/{repo}/commits', 'BitBucketController@getRepoCommits');

Route::get('test', function () {
    return "Test Successful!";
});
