<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| GitHub Control Routes
|--------------------------------------------------------------------------
*/

Route::get('organization', 'GitHubController@organizations');

Route::get('organization/{org}', 'GitHubController@organization');
Route::post('organization/add', 'GitHubController@addOrgUser');
Route::get('organization/{org}/remove/{user}', 'GitHubController@removeOrgUser');

Route::get('repository/{username}/{repo}/details', 'GitHubController@displayRepoDetails');
Route::get('repository/{username}/{repo}/commits', 'GitHubController@getRepoCommits');


Route::get('timer/{timerIDHash}/{contact}', 'Features\ContactTimerController@getTime');

Route::post('timer', 'Features\ContactTimerController@setTime');

Route::get('test', function () {
    return "Test Successful!";
});
