<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

// DataGrid API Routes

Route::get('/category-data', 'ApiController@categoryData');
Route::get('/lesson-data', 'ApiController@lessonData');
Route::get('/marketing-image-data', 'ApiController@marketingImageData');
Route::get('/profile-data', 'ApiController@profileData');
Route::get('/subcategory-data', 'ApiController@subcategoryData');
Route::get('/user-data', 'ApiController@userData');
Route::get('/widget-data', 'ApiController@widgetData');

// Feature DataGrid API Routes

Route::get('/timer-data', 'ApiController@timerData');

// Webhook API Routes

Route::post('/github/push/{repo_hash}', 'WebHookController@GitHubPush')->name('github.webhook');
Route::post('/bitbucket/push/{repo_hash}', 'WebHookController@BitBucketPush')->name('bitbucket.webhook');

// Module API Routes

Route::middleware('auth:api')->group(function () {

    Route::get('/github/user/repos', 'GitHubController@APIuserRepos');
    Route::get('/github/organization/{org}/repos', 'GitHubController@APIorgRepos');
    Route::get('/github/organization/{org}/members', 'GitHubController@APIorgMembers');

    Route::get('/bitbucket/user/repos', 'BitBucketController@APIuserRepos');
    Route::get('/bitbucket/organization/{org}/repos', 'BitBucketController@APIorgRepos');
    Route::get('/bitbucket/organization/{org}/members', 'BitBucketController@APIorgMembers');

    Route::get('/{source}/repository/{username}/{repo}/track', 'RepoController@trackRepo');

});

