<?php
namespace Deployer;

require 'recipe/laravel.php';
require 'recipe/yarn.php';

// Configuration

//set('bin/php', '/usr/local/bin/ea-php71');
set('composer_action', 'install');
set('composer_options', '{{composer_action}} --no-interaction');
set('http_user', 'devteamtools');

set('repository', 'ssh://git@bitbucket.org/dynamyx/devteam.git');
set('git_tty', true); // [Optional] Allocate tty for git on first deployment
add('shared_files', []);
add('shared_dirs', []);
add('writable_dirs', []);


// Hosts

// To Deploy: dep deploy production

host('odin')
    ->stage('production')
    ->user('devteamtools')
    ->set('deploy_path', '/home/devteamtools/laravel');


task('pwd', function () {
    $result = run('pwd');
    writeln("Current dir: $result");
});

// To Update: dep update odin

desc('Update Codebase');
task('update', function() {
    $output = run('cd {{deploy_path}}/current; git pull origin master');
    writeln('<info>' . $output . '</info>');
});

//host('beta.project.com')
//    ->stage('beta')
//    ->set('deploy_path', '/var/www/project.com');


// Tasks

//desc('Restart PHP-FPM service');
//task('php-fpm:restart', function () {
//    // The user must have rights for restart service
//    // /etc/sudoers: username ALL=NOPASSWD:/bin/systemctl restart php-fpm.service
//    run('sudo systemctl restart php-fpm.service');
//});
//after('deploy:symlink', 'php-fpm:restart');

after('deploy:update_code', 'yarn:install');

task('yarn:run-production', function() {
   run('cd {{release_path}} && {{bin/yarn}} run production');
});
after('yarn:install', 'yarn:run-production');

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.

before('deploy:symlink', 'artisan:migrate');
